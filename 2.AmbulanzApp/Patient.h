/**
(c) Hochschule Heilbronn
Fakultaet fuer Informatik 
Prof. Dr. R. Bendl


Einfuehrung in C++ (Medizinische Informatik Bachelor)

Rudimentaere Rumpfimplementierung einer Applikation
zur Demonstration grundlegender C++ Moeglichkeiten. 

Aus didaktischen Gruenden (reduzierte Komplexitaet
fuer Einsteiger) wurden nicht alle Features optimal 
implementiert. Die Module koennen sukzessive von Studierenden
erweitert und verbessert werden

Klasse Patient

In einem Patient-Objekt werden Name, Vorname,  ... eines
Patienten gespeichert.

Maintenance Info:
2017-09-25 R. Bendl
Modul erstellt

*/

#pragma once

#include <string>
#include "person.h"

class Patient : public Person
  {
  public:
    // es ist effizienter, die Datenelemente in einer Initialisierungsliste
    // zu initialisieren (nach dem Doppelpunkt) als im Rumpf der Methode
    // 
    Patient (int id, std::string name = " ", std::string vorname = " ", Person::Geschlecht geschlecht = Geschlecht::U) 
      : Person (name, vorname, geschlecht)
      {}
    Patient( const Patient & p) { ID = p.ID; arzt = p.arzt; krankenkasse = p.krankenkasse;}
     ~ Patient () {};

    int getID() {return ID;}
  private:
    int         ID;
    Person      arzt;
    std::string krankenkasse;
    Patient() {};
  };
