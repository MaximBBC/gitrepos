/**
(c) Hochschule Heilbronn
Fakultaet fuer Informatik 
Prof. Dr. R. Bendl


Einfuehrung in C++ (Medizinische Informatik Bachelor)

Rudimentaere Rumpfimplementierung einer Applikation
zur Demonstration grundlegender C++ Moeglichkeiten. 

Aus didaktischen Gruenden (reduzierte Komplexitaet
fuer Einsteiger) wurden nicht alle Features optimal 
implementiert. Die Module koennen sukzessive von Studierenden
erweitert und verbessert werden

Klasse PatientenAdmin

Klasse verwaltet die Patientendaten und stellt Funktionen
zur Manipulation der Daten zur Verfuegung

Maintenance Info:
2017-09-25 R. Bendl
Modul erstellt 

*/

// Standard Includes 
#include <iostream>
using namespace std;

// Eigene Includes
#include "PatientenAdmin.h"

void PatientenAdmin::start ()
  {
  if(!initialisiert)
    {
    // anlegen Patienten-Array oder andere sinnvolle Verwaltungsstrukturen
    initialisiert        = true;
    patienten            = new Patient* [arrayIncrement] ();
    laengePatientenArray = arrayIncrement;
    }
  // weitere Initialisierungen
  // z.B. Einlesen exisitierender Patienten aus externem Speicher ...
  }

void PatientenAdmin::aufnehmenPatient (std::string name, std::string vorname, Person::Geschlecht geschlecht)
  {
  if(!initialisiert) start ();
  if(anzahlPatienten < laengePatientenArray)
    { 
    int id = 0;
    Patient* pat = new Patient(id, name, vorname, geschlecht);
    patienten[anzahlPatienten] = pat;
    anzahlPatienten++;
    }
  }

void PatientenAdmin::zeigePatienten ()
  {
   cout << "Patienten:" << endl;
   cout << "Name" << "\t\t\t" << "Vorname" << "\t\t\t" << "Geschlecht" << endl;

   for(int i = 0; i < anzahlPatienten; i++)
     {
      Patient *pat = patienten[i];
      cout << pat->getName() << "\t\t\t" << pat->getVorname() << "\t\t\t" << pat->getGeschlecht() << endl;
     }
  }

